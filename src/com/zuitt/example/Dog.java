package com.zuitt.example;
//Child class of Animal
    //"extends" keyword that is used to inherit the properties and methods of parent class
public class Dog extends Animal{

    //properties
    private String breed;

    // constructor
    public Dog(){
        //super is to have direct access with the original constructor(PARENT CLASS)
        super();
        this.breed = "Chihuahua";
    }
    public Dog (String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    //Getter and setter
    public String getBreed() {
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    //Method
    public void speak (){
        System.out.println("Woof, woof");
    }

    public void call(){
        super.call();
        System.out.println("Hi! my name is: " + this.getName() + ", I am dog");
    }
}
