package com.zuitt.example;

//Interface Implementation
// this is where we implement our Action interface
public class Person implements Actions, Greetings{
    public void sleep(){
        System.out.println("ZzzzzzZZZzz....");
    }

    public void run () {
        System.out.println("Running");
    }

    public void morningGreet(){
        System.out.println("Top of the morning y'all");
    }

    public void holidayGreet(){
        System.out.println("Happy holidays");
    }
}
