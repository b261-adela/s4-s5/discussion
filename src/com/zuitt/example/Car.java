package com.zuitt.example;

public class Car {

    //Access modifier
        // There are used to restrict the scope of a class, constructor, variable, method or data

        // Four types of access modifier
            // 1. Default - No keyword indicated (accessibility is within the package).
            // 2. Private - Properties or methods can only be accessed within the class.
            // 3. Protected - Properties and method are only accessible by the class of the same package and the subclass in any package.
            // 4. Public - Properties and methods can be accessed from anywhere.

    // Class Creation
        // 4 parts of class creation
            //1. Properties - characteristics of an object; also known as variable

        private String name;
        private String brand;
        private int yearOfMake;

        //Makes additional component of a Car
        private Driver driver;

        //2. Constructor - used to create/instantiate an object
        // a. empty constructor - creates object that doesn't have any argument/ parameters. Also referred as default constructor

        public Car() {
            this.yearOfMake = 2000;
            this.driver = new Driver("Alejandro");
        };


        //b. parameterized constructor - create an object with arguments/parameters.

        public Car(String name, String brand, int yearOfMake){
            //this.name > property
            //name > parameter
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
            this.driver = new Driver("Alejandro");
        }

        //3. Getters and Setters - get and set the values of each property of an object

    // Getter - retrieves the value of the instantiated object
        public String getName(){
            return this.name;
        }

        public String getBrand(){
            return this.brand;
        }

        public int getYearOfMake(){
            return this.yearOfMake;
        }

        public String getDriverName(){
            return this.driver.getName();
        }

        // Setters -used to change the default value of an instantiated object

        public void setName(String name){
            this.name = name;
        }

        public void setBrand(String brand){
            this.brand = brand;
        }

        public void setYearOfMake(int yearOfMake){
            // can also be modified to add validation
            if(yearOfMake <= 2023){
                this.yearOfMake = yearOfMake;
            }

        }

        public void setDriver(String driver){
            this.driver.setName(driver);
        }

        //4. Methods - function that an object can perform (actions). this is optional
        public void drive(){
            System.out.println("This car is running. skrr skrr");
        }
}

















